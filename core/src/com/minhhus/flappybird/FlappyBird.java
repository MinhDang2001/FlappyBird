package com.minhhus.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.minhhus.flappybird.model.Point;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FlappyBird extends ApplicationAdapter {
	SpriteBatch batch;
	Texture background;
	Texture[] birds;

	Texture topTube;
	Texture bottomTube;
	Texture gameOver;
	Texture backgroundHighScore;
	Texture soundOff;
	Texture soundOn;

	Point LatestTouchPoint = new Point();
	boolean soundIsPlaying;

	int flapState = 0;

	int widthScreen;
	int heightScreen;

	float birdY;
	float birdX;
	//if gameState == 0 : status has not started , if gameState == 1 : game is playing , if gameState == 2 : game is finish
	int gameState = 0;

	float velocity = 0;
	float gravity = 1.5F;

	int numberOfTube = 4;
	float flyingRange = 400;

	float[][] gap = new float[2][numberOfTube];
	float tubeX[] = new float[numberOfTube];
	float tubeY;
	// Velocity of tube
	float tubeVelocity = 4;
	float distanceBetweenTubes;

	Random random = new Random();

	//wrap object in game to catch events
	Circle birdWrap;
	Rectangle[] topTubeWrap;
	Rectangle[] bottomTubeWrap;

	private int score;
	private int scoreTube;
	BitmapFont bitmapFontScore;
	BitmapFont bitmapFontHighScore;

	private Sound jumpOver1TubeSound;
	private Sound dieSound;
	Preferences highScoreRef;
	Preferences soundSettingRef;

	@Override
	public void create () {
		batch = new SpriteBatch();
		background = new Texture("bg.png");
		birds = new Texture[2];
		birds[0] = new Texture("bird.png");
		birds[1] = new Texture("bird2.png");
		bottomTube = new Texture("bottomtube.png");
		topTube = new Texture("toptube.png");
		gameOver = new Texture("gameOver.png");
		backgroundHighScore = new Texture("background_high_score.png");

		highScoreRef = Gdx.app.getPreferences("highScore.pref");
		soundSettingRef = Gdx.app.getPreferences("setting.pref");

		jumpOver1TubeSound = Gdx.audio.newSound(Gdx.files.internal("jump_over_1_tube_sound.mp3"));
		dieSound = Gdx.audio.newSound(Gdx.files.internal("die_sound.mp3"));
		soundOn = new Texture("sound_on.png");
		soundOff = new Texture("sound_off.png");
		soundIsPlaying = readSettingSound();


		widthScreen = Gdx.graphics.getWidth();
		heightScreen = Gdx.graphics.getHeight();

		distanceBetweenTubes = widthScreen*2/3;

		bitmapFontScore = new BitmapFont();
		bitmapFontScore.setColor(Color.WHITE);
		bitmapFontScore.getData().scale(5);

		bitmapFontHighScore = new BitmapFont();
		bitmapFontHighScore.setColor(Color.WHITE);
		bitmapFontHighScore.getData().scale(2.5f);

		startGame();
	}

	@Override
	public void render () {
		batch.begin();

		initView();
		logicGame();

		batch.end();
	}

	private void initView(){
		batch.draw(background,0,0, widthScreen,heightScreen);

		for (int i = 0; i <numberOfTube; i++) {

			// den khi nao vtri theo truc tuong doi theo truc x cua cac cot nho hon vi tri cua cot chinh giua , thi reset
			if (tubeX[i]< - topTube.getWidth()){
				tubeX[i] = tubeX[i] + (numberOfTube) * distanceBetweenTubes;
			}
			//mount the top tube on top và random position
			batch.draw(topTube,tubeX[i],tubeY+gap[0][i]);
			//mount the bottom tube on bottom và random position
			batch.draw(bottomTube,tubeX[i],tubeY-bottomTube.getHeight()-gap[1][i]);

			//wrap tube
			topTubeWrap[i] = new Rectangle(tubeX[i],
					tubeY+gap[0][i],
					topTube.getWidth(),
					topTube.getHeight());

			bottomTubeWrap[i] = new Rectangle(tubeX[i],
					tubeY-bottomTube.getHeight()-gap[1][i],
					bottomTube.getWidth(),
					bottomTube.getHeight());
		}

		if (flapState == 0){
			flapState = 1;
		}else flapState = 0;

		batch.draw(birds[flapState],birdX,birdY);
		bitmapFontScore.draw(batch,
				String.valueOf(score),
				widthScreen/2- bitmapFontScore.getScaleX()/2,
				heightScreen*4/5);

	}

	private void logicGame(){
		if (gameState == 1){

			if (Gdx.input.justTouched()){
				// altitude per press
				velocity = -20;
			}

			if (tubeX[scoreTube] < widthScreen/2){
				score++;
				if (soundIsPlaying) jumpOver1TubeSound.play();
				if (scoreTube < numberOfTube - 1) scoreTube++;
				else scoreTube = 0;
			}

			for (int i = 0; i <numberOfTube; i++) {
				// give the tube motion
				tubeX[i] = tubeX[i] - tubeVelocity;
			}

			// fall without touching
			if (birdY > 0 || velocity < 0){
				velocity = velocity + gravity;
				birdY = birdY - velocity;
			}

			birdWrap.set(widthScreen/2,
					birdY + birds[flapState].getWidth()/2,
					birds[flapState].getWidth()/2);

			//catch event when bird collision tube
			for (int i = 0; i < numberOfTube; i++) {
				if (Intersector.overlaps(birdWrap,topTubeWrap[i])||Intersector.overlaps(birdWrap,bottomTubeWrap[i])) {
					gameState = 2;
					if (soundIsPlaying) dieSound.play();
				}
			}

		}else if (gameState == 0) {
			if (soundIsPlaying)
				batch.draw(soundOn,
						widthScreen * 8 / 10,
						heightScreen * 9 / 10,
						soundOn.getWidth(),
						soundOn.getHeight());
			else batch.draw(soundOff,
					widthScreen * 8 / 10,
					heightScreen * 9 / 10,
					soundOff.getWidth(),
					soundOff.getHeight());

			if (Gdx.input.justTouched()) {
				LatestTouchPoint.setX(Gdx.input.getX());
				LatestTouchPoint.setY(Gdx.input.getY());

				if (LatestTouchPoint.getX() > widthScreen * 8 / 10 && LatestTouchPoint.getX() < widthScreen * 8 / 10 + soundOff.getWidth() &&
						LatestTouchPoint.getY() < heightScreen / 10 && LatestTouchPoint.getY() > heightScreen / 10 - soundOff.getHeight()) {
					soundIsPlaying = !soundIsPlaying;
					if (soundIsPlaying){
						batch.draw(soundOn,
								widthScreen * 8 / 10,
								heightScreen * 9 / 10,
								soundOn.getWidth() ,
								soundOn.getHeight());
					} else{
						batch.draw(soundOff,
								widthScreen * 8 / 10,
								heightScreen * 9 / 10,
								soundOn.getWidth(),
								soundOn.getHeight());
					}
					writeSettingSound();
					// change status game
				}else gameState = 1;
			}
		}else {
			batch.draw(gameOver,
					widthScreen/2-(gameOver.getWidth()*1/3),
					heightScreen/2+gameOver.getHeight()/2,
					gameOver.getWidth()*2/3,
					gameOver.getHeight()*2/3);

			if (soundIsPlaying)
				batch.draw(soundOn,
						widthScreen*8/10,
						heightScreen*9/10,
						soundOn.getWidth(),
						soundOn.getHeight());
			 else 	batch.draw(soundOff,
					widthScreen*8/10,
					heightScreen*9/10,
					soundOff.getWidth(),
					soundOff.getHeight());

			// fall when die
			if (birdY > 0 || velocity < 0){
				velocity = velocity + gravity;
				birdY = birdY - velocity;
			}

			logicScore();

			flapState = 0;

			if (Gdx.input.justTouched()) {
				LatestTouchPoint.setX(Gdx.input.getX());
				LatestTouchPoint.setY(Gdx.input.getY());

				if (LatestTouchPoint.getX() > widthScreen * 8 / 10 && LatestTouchPoint.getX() < widthScreen * 8 / 10 + soundOff.getWidth() &&
						LatestTouchPoint.getY() < heightScreen / 10 && LatestTouchPoint.getY() > heightScreen / 10 - soundOff.getHeight()) {
					soundIsPlaying = !soundIsPlaying;
					if (soundIsPlaying){
						batch.draw(soundOn,
								widthScreen * 8 / 10,
								heightScreen * 9 / 10,
								soundOn.getWidth(),
								soundOn.getHeight());
					} else{
						batch.draw(soundOff,
								widthScreen * 8 / 10,
								heightScreen * 9 / 10,
								soundOn.getWidth(),
								soundOn.getHeight());
					}

					writeSettingSound();
					// change status game
				} else{
					startGame();
					gameState = 0;
				}
			}

		}
	}

	private void startGame(){
		birdY = heightScreen/2 - birds[flapState].getHeight()/2;
		birdX = widthScreen/2 - birds[flapState].getWidth()/2;
		scoreTube = 0;
		score = 0;
		velocity = 0;

		topTubeWrap = new Rectangle[numberOfTube];
		bottomTubeWrap = new Rectangle[numberOfTube];
		birdWrap = new Circle();

		for (int i = 0 ; i < numberOfTube ; i++){
			gap[0][i] = randomFlyingRange();
			gap[1][i] = flyingRange-gap[0][i];
			// set vi tri tuong doi cho tung cot , tinh tu vi tri center dau tien
			tubeX[i] = widthScreen/2-topTube.getWidth()/2 + (i+1) * distanceBetweenTubes;

			topTubeWrap[i] = new Rectangle();
			bottomTubeWrap[i] = new Rectangle();
		}
		tubeY = heightScreen/2;
	}

	private void logicScore() {

		batch.draw(backgroundHighScore,
				widthScreen/2-backgroundHighScore.getWidth()*5/2,
				heightScreen/2-backgroundHighScore.getHeight()*3,
				backgroundHighScore.getWidth()*5,
				backgroundHighScore.getHeight()*5);


//		resetHighScore(highScoreRef);
		boolean changeHighScore = false;
		int tempScore = 0;
		Map<String,Integer> highScoreLatest = readHighScore(highScoreRef);
		for (Map.Entry<String,Integer> item : highScoreLatest.entrySet()){
			if (changeHighScore){
				int currentScore = item.getValue();
				item.setValue(tempScore);
				tempScore = currentScore;
			} else if (score >  item.getValue()||item.getValue()==null){
				changeHighScore = true;
				tempScore = item.getValue();
				item.setValue(score);
			}else if (score ==  item.getValue()) break;
		}

		if (changeHighScore) writeHighScore(highScoreLatest, highScoreRef);

		bitmapFontHighScore.draw(
				batch,
				"Top",
				widthScreen/2 - widthScreen*2.5f/10 ,
				heightScreen*57/100);
		bitmapFontHighScore.draw(
				batch,
				"Score",
				widthScreen/2 + widthScreen*0.3f/10,
				heightScreen*57/100);


		bitmapFontHighScore.draw(
				batch,
				"1",
				widthScreen/2 - widthScreen*2/10 ,
				heightScreen*52/100);
		bitmapFontHighScore.draw(
				batch,
				String.valueOf(highScoreLatest.get("1")),
				widthScreen/2 + widthScreen/10,
				heightScreen*52/100);

		bitmapFontHighScore.draw(
				batch,
				"2",
				widthScreen/2 - widthScreen*2/10 ,
				heightScreen*47/100);
		bitmapFontHighScore.draw(
				batch,
				String.valueOf(highScoreLatest.get("2")),
				widthScreen/2 + widthScreen/10,
				heightScreen*47/100);

		bitmapFontHighScore.draw(
				batch,
				"3",
				widthScreen/2 - widthScreen*2/10 ,
				heightScreen*42/100);
		bitmapFontHighScore.draw(
				batch,
				String.valueOf(highScoreLatest.get("3")),
				widthScreen/2 + widthScreen/10,
				heightScreen*42/100);
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}

	private float randomFlyingRange(){
		return random.nextInt(401);
	}

	private void writeHighScore(Map<String,Integer> highScoreLatest , Preferences highScoreRef)  {
		for (Map.Entry<String,Integer> item : highScoreLatest.entrySet()){
			highScoreRef.putInteger(item.getKey(),item.getValue());
		}
		highScoreRef.flush();
	}

	private Map<String,Integer> readHighScore(Preferences highScoreRef){
		Map<String, Integer> result = new HashMap<>();

		result.put("1",highScoreRef.getInteger("1"));
		result.put("2",highScoreRef.getInteger("2"));
		result.put("3",highScoreRef.getInteger("3"));

		return result;
	}

	private void resetHighScore(Preferences highScoreRef){
		highScoreRef.putInteger("1",0);
		highScoreRef.putInteger("2",0);
		highScoreRef.putInteger("3",0);

		highScoreRef.flush();
	}

	private void writeSettingSound(){
		soundSettingRef.putBoolean("isOpen",soundIsPlaying);
		soundSettingRef.flush();
	}

	private boolean readSettingSound(){
		return soundSettingRef.getBoolean("isOpen");
	}

	private void resetSettingSound(){
		soundSettingRef.putBoolean("isOpen",true);
		soundSettingRef.flush();
	}
}
